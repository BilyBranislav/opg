package prax.bookAndLibrary;

public class Book {

    private String name;
    private String authors[];
    private String characters[];
    private double rating;
    private String genre;

    public Book(String name, String[] authors, String[] characters, double rating, String genre) {
        this.name = name;
        this.authors = authors;
        this.characters = characters;
        this.rating = rating;
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String[] getCharacters() {
        return characters;
    }

    public void setCharacters(String[] characters) {
        this.characters = characters;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        if(rating >= 0 && rating <= 10) {
            this.rating = rating;
        } else {
            System.err.print("Your rating needs to be [0, 10]");
        }
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean hasAnAuthor(String searchedAuthor) {
        for (String author :
                authors) {
            if(author.equals(searchedAuthor)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name of the book is " + name + "\n");
        sb.append("Author/s is/are: ");
        for (String author :
                authors) {
            sb.append(author + " ");
        }

        sb.append("\nCharacters in the book are : ");
        for (String character :
                characters) {
            sb.append(character + " ");
        }

        sb.append("\nRating of this book is " + rating);
        sb.append("\nGenre of this book is " + genre);
        sb.append("\n");
        return sb.toString();
    }
}
