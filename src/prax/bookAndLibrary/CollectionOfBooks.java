package prax.bookAndLibrary;

import java.util.HashSet;
import java.util.Set;

public class CollectionOfBooks {
    private Set<Book> books;

    public CollectionOfBooks() {
        this.books = new HashSet<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(Book bookToRemove) {
        books.removeIf(book -> book.equals(bookToRemove));


//        books.removeIf(new Predicate<Book>() {
//            @Override
//            public boolean test(Book book) {
//                return book.equals(bookToRemove);
//            }
//        });
    }

    public void removeBook(String nameOfBook) {
        books.removeIf(book -> book.getName().equals(nameOfBook));
    }

    public void printAll() {
        if(books.isEmpty()) {
            System.out.println("No books to print.");
        } else {
            for (Book book : books) {
                printBook(book);
            }
        }
    }

    public void printByAuthor(String author) {
        for (Book book : books) {
            if (book.hasAnAuthor(author)) {
                printBook(book);
            }
        }
    }

    public void printByGenre(String genre) {
        for (Book book :
                books) {
            if (book.getGenre().equals(genre)) {
                printBook(book);
            }
        }
    }
    public void printBook(Book book) {
        System.out.println(book.toString());
    }

}


