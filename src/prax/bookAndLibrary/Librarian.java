package prax.bookAndLibrary;

import java.util.ArrayList;

public class Librarian {

    private Library library;
    private ArrayList<Book> books;


    public Librarian(Library library) {
        this.library = library;
        this.books = library.getLibrary();
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public void printEverything() {
        StringBuilder sb = new StringBuilder();
        for (Book book :
                library.getLibrary()) {
            sb.append(book.toString() + "\n");
        }
        System.out.println(sb.toString());
    }

    public void printTitles() {
        System.out.println("Printing books by title: ");
        StringBuilder sb = new StringBuilder();
        for (Book book :
                library.getLibrary()) {
            sb.append(book.getName() + "\n");
        }
        System.out.println(sb.toString() + "\n");
    }

    public void printByGenre(String genre) {
        System.out.println("Printing books by genre: ");
        for (Book book :
                books) {
            if (book.getGenre().equals(genre)) {
                System.out.println(book.toString() + "\n");
            }
        }
    }

    public void printByAuthor(String author) {
        System.out.println("Printing books by author: ");
        for (Book book :
                books) {
            if(book.hasAnAuthor(author)) {
                System.out.println(book.toString() + "\n");
            }
        }
    }

    public void printByRating(double lowestPoint, double highestPoint) {
        System.out.println("Printing books by rating: ");
        for (Book book :
                books) {
            if(book.getRating() >= lowestPoint && book.getRating() <= highestPoint) {
                System.out.println(book.toString() + "\n");
            }
        }
    }










}
