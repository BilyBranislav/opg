package prax.bookAndLibrary;

import java.util.ArrayList;

public class Library {
    private ArrayList<Book> library = new ArrayList<>();

    public ArrayList<Book> getLibrary() {
        return library;
    }

    public void setLibrary(ArrayList<Book> library) {
        this.library = library;
    }

    public void addBook(Book book) {
        library.add(book);
        System.out.println("Book " + book.getName() + " was added.\n");
    }

    public void removeBook(Book book) {
        library.remove(book);
        System.out.println("Book " + book.getName() + " was deleted.\n");
    }

    public void removeBook(String name) {
        Book book = findBook(name);
        if(book != null) {
            removeBook(book);
        } else {
            System.out.println("Book was not found\n");
        }
    }

    private Book findBook(String name) {
        for (Book book :
                library) {
            if (book.getName().equals(name)) {
                return book;
            }
        }
        return null;
    }
}
