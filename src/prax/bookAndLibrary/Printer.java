package prax.bookAndLibrary;

public class Printer {

    public Book printHarryPotter1() {
        return new Book("Harry Potter 1", new String[] {"J.K.Rowling"},
                new String[] {"Harry Potter", "Hermione Granger", "Ron Wesley"}, 6.2, "fantasy");
    }

    public Book printHarryPotter2() {
        return new Book("Harry Potter 2", new String[] {"J.K.Rowling"},
                new String[] {"Harry Potter", "Hermione Granger", "Ron Wesley"}, 6.4, "fantasy");
    }

    public Book printWinnetou() {
        return new Book("Winnetou", new String[] {"Karl May"},
                new String[] {"Winnetou, The other guy"}, 8.3, "western");
    }

    public Book printThinkingAboutIt() {
        return new Book("Thinking about it only makes it worse", new String[] {"David Mitchell"},
                new String[]{"David Mitchell"}, 9.1, "soap box");
    }


}
