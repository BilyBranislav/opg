package prax.bookAndLibrary;

public class Start {
    public static void main(String[] args) {

        Printer printer = new Printer();
        Library library = new Library();
        Librarian Brian = new Librarian(library);
        CollectionOfBooks collectionOfBooks = new CollectionOfBooks();

        Book harryPotter1 = printer.printHarryPotter1();
        Book winnetou = printer.printWinnetou();
        Book thinkingAboutIt = printer.printThinkingAboutIt();

//        library.addBook(harryPotter1);
//        library.addBook(winnetou);
//        library.addBook(thinkingAboutIt);
//
//        Brian.printEverything();
//
//        library.removeBook("Winnetou");
//        library.removeBook(harryPotter1);
//
//        Brian.printEverything();

        collectionOfBooks.addBook(harryPotter1);
        Book harryPotter2 = printer.printHarryPotter2();
        collectionOfBooks.addBook(harryPotter2);
        collectionOfBooks.printAll();
        collectionOfBooks.printByAuthor("J.K.Rowling");
        collectionOfBooks.removeBook(harryPotter1);
        collectionOfBooks.removeBook("Harry Potter 2");
        collectionOfBooks.printAll();

    }
}
