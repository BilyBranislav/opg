package prax.dedicnost;

import org.jetbrains.annotations.NotNull;
import prax.dedicnost.knihy.AudioKniha;
import prax.dedicnost.knihy.EKniha;
import prax.dedicnost.knihy.Kniha;
import prax.dedicnost.knihy.PapierovaKniha;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 16.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Start {
    public static void main(String[] args) {
//        AudioKniha zavarameNaChalupe = new AudioKniha("Zavarame na chalupe", 1990,new String[]{"Jano", "Jaro", "Julo"},
//                                            new String[]{"mrkva", "cibula", "uhorka", "kopor"},"kucharka",  9.2, 100, 5);
//
//        PapierovaKniha harryPotter = new PapierovaKniha("Harry Potter a kamen mudrcov", 2000,  new String[]{"J.K. Rowling"},
//                                      new String[]{"Harry", "Ron", "Hermiona", "Voldy"}, "rozpravka", 7.2, 4, Vazba.LEPORELO);
//
//        EKniha panPrstenov = new EKniha("Pan prstenov a dve veze",1975, new String[]{"J.R.R. Tolkien"},
//                                      new String[]{"Gandalf", "Frodo", "Sam", "Golum"}, "fantasy", 10);

        ZoznamKnih z = new ZoznamKnih();
        try (Scanner sc = new Scanner(new FileReader("subor.txt"))) {
            int pocetKnih = sc.nextInt();
            for (int i = 0; i <= pocetKnih; i++) {
                String typKnihy = sc.nextLine();
                switch (typKnihy) {
                    case "audio": {
                        AudioKniha audioKniha = spravAudioknihu(sc);
                        z.vlozKnihu(audioKniha);
                        break;
                    }
                    case "papierova": {
                        PapierovaKniha papierovaKniha = spravPapierovuKnihu(sc);
                        z.vlozKnihu(papierovaKniha);
                        break;

                    }
                    case "elektronicka": {
                        EKniha eKniha = spravElektroknihu(sc);
                        z.vlozKnihu(eKniha);
                        break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        z.vypisVsetko();
    }

    private static AudioKniha spravAudioknihu(Scanner sc) {
        return new AudioKniha(sc);
    }

    private static PapierovaKniha spravPapierovuKnihu(Scanner sc) {
        return new PapierovaKniha(sc);
    }

    private static EKniha spravElektroknihu(Scanner sc) {
        return new EKniha(sc);
    }

}
