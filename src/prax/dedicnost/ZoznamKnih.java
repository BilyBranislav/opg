package prax.dedicnost;

import prax.dedicnost.knihy.Kniha;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 16.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class ZoznamKnih {
    private Kniha[] knihy;

    public ZoznamKnih() {
        knihy = new Kniha[0];
    }

    public void vlozKnihu (Kniha kniha) {
        Kniha[] novePole = new Kniha[knihy.length + 1];
        System.arraycopy(knihy,0, novePole,0, knihy.length);
        knihy = novePole;
        knihy[knihy.length -1] = kniha;
    }

    public void vymazKnihu (Kniha kniha) {
        int index = -1;
        for (int i = 0; i < knihy.length; i++) {
            if (kniha.equals(knihy[i])) {
                index = i;
                break;

            }
        }

        if(index >= 0) {
            Kniha[] novePole = new Kniha[knihy.length-1];
            System.arraycopy(knihy, 0, novePole, 0, index);
            System.arraycopy(knihy, index +1, novePole, index, novePole.length - index);
            knihy = novePole;
        }
    }

    public void vymazKnihu (String nazovKnihy){
        for (int i = 0; i < knihy.length; i++) {
            if (nazovKnihy.equals(knihy[i].getNazovKnihy())){
                this.vymazKnihu(knihy[i]);
            }
        }
    }

    public void vypisVsetko () {
        for (int i = 0; i < knihy.length; i++) {
            System.out.println(knihy[i].toString());
        }
    }

    public void vypisPodlaZanru (String zaner) {
        for (int i = 0; i < knihy.length; i++) {
            if(knihy[i].getZaner().equals(zaner)){
                System.out.println(knihy[i].toString());
            }
        }
    }

    public void vypisPodlaAutora (String autor) {
        for (int i = 0; i < knihy.length; i++) {
            if(knihy[i].mamAutora(autor)){
                System.out.println(knihy[i].toString());
            }
        }
    }

    public void vypisPodlaHodnotenia (double hodnotenieOd, double hodnotenieDo) {
        for (int i = 0; i < knihy.length; i++) {
            if(knihy[i].getHodnotenie() >= hodnotenieOd && knihy[i].getHodnotenie() <= hodnotenieDo) {
                System.out.println(knihy[i].toString());
            }
        }
    }
}
