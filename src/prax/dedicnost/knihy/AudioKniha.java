package prax.dedicnost.knihy;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 16.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class AudioKniha extends Kniha {

    private double dlzka;
    private double zalozka;

    public AudioKniha(String nazovKnihy, int rokVydania, String[] autori, String[] postavy, String zaner, double hodnotenie, double dlzka, double zalozka) {
        super(nazovKnihy, rokVydania, autori, postavy, zaner, hodnotenie);
        this.dlzka = dlzka;
        this.zalozka = zalozka;
    }

    public AudioKniha(Scanner sc) {
        super(sc);
        dlzka = Double.parseDouble(sc.nextLine());
        zalozka = Double.parseDouble(sc.nextLine());
    }

    public double getDlzka() {
        return dlzka;
    }

    public void setDlzka(int dlzka) {
        this.dlzka = dlzka;
    }

    public double getZalozka() {
        return zalozka;
    }

    public void setZalozka(int zalozka) {
        this.zalozka = zalozka;
    }

    @Override
    public String dodatocneInformacie() {
        return "Dlzka knihy: " + dlzka + ", zalozka sa nachadza na: " + zalozka;
    }

}
