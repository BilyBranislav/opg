package prax.dedicnost.knihy;

import prax.dedicnost.knihy.Kniha;

import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 16.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class EKniha extends Kniha {

    private String nazovPC;
    private String cesta;
    private long velkostSuboru;

    public EKniha(String nazovKnihy, int rokVydania, String[] autori, String[] postavy, String zaner, double hodnotenie, String nazovPC, String cesta, long velkostSuboru) {
        super(nazovKnihy, rokVydania, autori, postavy, zaner, hodnotenie);
        this.nazovPC = nazovPC;
        this.cesta = cesta;
        this.velkostSuboru = velkostSuboru;
    }

    public EKniha(Scanner sc) {
        super(sc);
        nazovPC = sc.nextLine();
        cesta = sc.nextLine();
        velkostSuboru = Long.parseLong(sc.nextLine());
    }

    @Override
    public String dodatocneInformacie() {
        return "V pocitaci: " + nazovPC + ", v subore : " + cesta + ", " + " velkost: " + velkostSuboru + "KB";
    }
}
