package prax.dedicnost.knihy;

import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 16.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public abstract class Kniha {

    private String nazovKnihy;
    private int rokVydania;
    private String[] autori;
    private String[] postavy;
    private String zaner;
    private double hodnotenie;

    public Kniha() {
        this.hodnotenie = 5.0;
    }

    public Kniha(String nazovKnihy, int rokVydania, double hodnotenie) {
        this.nazovKnihy = nazovKnihy;
        this.rokVydania = rokVydania;
        this.hodnotenie = hodnotenie;
    }

    public Kniha(Scanner sc) {
        nazovKnihy = sc.nextLine();
        rokVydania = Integer.parseInt(sc.nextLine());
        autori = ziskajZoSuboruPodlaPoctu(sc);
        postavy = ziskajZoSuboruPodlaPoctu(sc);
        zaner = sc.nextLine();
        hodnotenie = Double.parseDouble(sc.nextLine());
    }

    public Kniha(String nazovKnihy, int rokVydania, String[] autori, String[] postavy, String zaner, double hodnotenie) {
        this.nazovKnihy = nazovKnihy;
        this.rokVydania = rokVydania;
        this.autori = autori;
        this.postavy = postavy;
        this.zaner = zaner;
        if (hodnotenie < 0 || hodnotenie > 10) {
            this.hodnotenie = 5.0;
        } else {
            this.hodnotenie = hodnotenie;
        }
    }

    public String getNazovKnihy() {
        return nazovKnihy;
    }

    public void setNazovKnihy(String nazovKnihy) {
        this.nazovKnihy = nazovKnihy;
    }

    public int getRokVydania() {
        return rokVydania;
    }

    public void setRokVydania(int rokVydania) {
        if (rokVydania >= 1443) {
            this.rokVydania = rokVydania;
        } else {
            System.err.println("Nemozes mat taku staru knihu, este nevynasli knihtlac.");
        }
    }

    public String[] getAutori() {
        return autori;
    }

    public void setAutori(String[] autori) {
        this.autori = autori;
    }

    public String[] getPostavy() {
        return postavy;
    }

    public void setPostavy(String[] postavy) {
        this.postavy = postavy;
    }

    public String getZaner() {
        return zaner;
    }

    public void setZaner(String zaner) {
        this.zaner = zaner;
    }

    public double getHodnotenie() {
        return hodnotenie;
    }

    public void setHodnotenie(double hodnotenie) {
        if (hodnotenie < 0 || hodnotenie > 10) {
            System.err.println("Hodnotenie " + hodnotenie + " je mimo povoleny rozsah [0, 10]");
        } else {
            this.hodnotenie = hodnotenie;
        }
    }

    public boolean mamAutora (String autor) {
        for (int i = 0; i < autori.length; i++) {
            if(autori[i].equals(autor)){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(nazovKnihy);
        sb.append(", rok vydania: ").append(rokVydania);
        sb.append(", autori: ");
        for (int i = 0; i < autori.length; i++) {
            sb.append(autori[i]).append(", ");
        }

        sb.append("\n");
        sb.append("hlavne postavy: ");
        for (int i = 0; i < postavy.length; i++) {
            sb.append(postavy[i]);
            if (i < postavy.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("\n");
        sb.append("zaner: ").append(zaner);
        sb.append(", hodnotenie: ").append(hodnotenie).append("\n");


        return sb.toString() + dodatocneInformacie() + "\n";
    }

    public abstract String dodatocneInformacie();

    private String [] ziskajZoSuboruPodlaPoctu(Scanner sc) {
        int pocetInformacii = Integer.parseInt(sc.nextLine());
        String [] informacieZoSuboru = new String[pocetInformacii];
        for (int i = 0; i < pocetInformacii; i++) {
            informacieZoSuboru[i] = sc.nextLine();
        }
        return informacieZoSuboru;
    }
}