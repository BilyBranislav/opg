package prax.dedicnost.knihy;

import prax.dedicnost.Vazba;
import prax.dedicnost.knihy.Kniha;

import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 16.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class PapierovaKniha extends Kniha {

    private int cisloPolicky;
    private Vazba vazba;

    public PapierovaKniha(String nazovKnihy, int rokVydania, String[] autori, String[] postavy, String zaner, double hodnotenie, int cisloPolicky, Vazba vazba) {
        super(nazovKnihy, rokVydania, autori, postavy, zaner, hodnotenie);
        this.cisloPolicky = cisloPolicky;
        this.vazba = vazba;
    }

    public PapierovaKniha(Scanner sc) {
        super(sc);
        cisloPolicky = Integer.parseInt(sc.nextLine());
        vazba = Vazba.valueOf(sc.nextLine());
    }

    @Override
    public String dodatocneInformacie() {
        return "Na policke cislo " + cisloPolicky + ", vazba: " + vazba;
    }



}
