package theory.collections;

import java.util.*;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 15.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie1 {

    public static void main(String[] args) {

        long cislo = nacitajCislo();
        long pred = System.currentTimeMillis();
        Set<Long> delitele = najdiDelitele(cislo);
        long po = System.currentTimeMillis() - pred;
        System.out.println(delitele.toString());
        System.out.println(po);
    }

    public static long nacitajCislo() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLong();
    }

    public static Set<Long> najdiDelitele(long cislo) {
        //Delitele budu usporiadane vdaka vyuziti TreeSet
        Set<Long> delitele = new TreeSet<>();
        for (long i=1; i * i <=cislo; i++) {
            //Ak su delitele
            if (cislo % i == 0) {
                //Pridame cislo aj vysledok delenia cislo / i, ak su rovnake prida iba jedno lebo je to Set
                delitele.add(i);
                delitele.add(cislo / i);
            }
        }
        return delitele;
    }

    public static Set<Long>najdiDelitelePomaly(long cislo) {
        Set<Long> delitele = new TreeSet<>();
        for (long i = cislo; i > 0 ; i--) {
            if(cislo % i == 0) {
                delitele.add(i);
            }
        }
        return delitele;
    }
}
