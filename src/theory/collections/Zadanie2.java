package theory.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashSet;


/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 15.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie2 {

    private static BufferedReader br;

    public static void main(String[] args) {
        LinkedHashSet<Long> prvoCisla = new LinkedHashSet<>();
        Long cislo = nacitanieCisla();
        if(cislo != null) {
            for (long i = 0; i <= cislo; i++) {
                if(jePrvocislo(i)) {
                    prvoCisla.add(i);
                }
            }
            vypisPrvocisla(prvoCisla);
        } else {
            System.out.println("Ziadne cislo sa nenaslo alebo subor neexistuje");
        }
    }

    public static Long nacitanieCisla() {
        try {
            br = new BufferedReader(new FileReader("prvoCislo.txt"));
            String vstup = br.readLine();
            br.close();
            return Long.parseLong(vstup);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean jePrvocislo(long cislo) {
        if (cislo % 2 ==0) return false;
        //Ak nie je parne
        for(long i = 3; i * i <= cislo; i += 2) {
            if(cislo % i ==0)
                return false;
        }
        return true;
    }

    public static void vypisPrvocisla(LinkedHashSet<Long> prvoCisla) {
        int poradie = 1;
        for (long prvoCislo :
                prvoCisla) {
            System.out.print(prvoCislo + ", ");
            if(poradie % 3 == 0) {
                System.out.println();
            }
            poradie++;
        }
    }
}