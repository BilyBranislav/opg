package theory.collections;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 15.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie3 {

    public static void main(String[] args) {
        System.out.println("Zadavajte mena ziakov, prestante stlacenim ENTER");

        Scanner sc = new Scanner(System.in);
        Queue<Ziak> ziaci = new LinkedList<>();
        String vstupMenoZiaka;
        int poradieZiaka = 1;


        while (!(vstupMenoZiaka = sc.nextLine()).equals("")) {
            ziaci.add(new Ziak(vstupMenoZiaka, poradieZiaka));
            poradieZiaka++;
        }

        while (ziaci.size() > 0) {
            System.out.println(ziaci.poll());
        }
    }
}

class Ziak {
    private String meno;
    private int poradie;

    Ziak(String meno, int poradie) {
        this.meno = meno;
        this.poradie = poradie;
    }

    public String toString() {
        return meno +" ide odpovedat ako " + poradie + ". ziak";
    }
}
