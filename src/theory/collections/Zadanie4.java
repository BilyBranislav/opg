package theory.collections;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 15.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie4 {
    public static void main(String[] args) {
        int pocetUcebnic = 20;

        String [] realnyVojaci = {"Andou Vanessa", "Clark Dakota Grant", "Andrews Michael Quincey", "Arnold Andi Kay",
                          "Arriaga Jonathan L", "Ashley Alexander Stephon", "Bakergriffis Latrese Shawan",
                          "Barnes Shane Bronza", "Bassett Thomas Lance", "Bennett Ayonna Danisha Shan",
                          "Bernardo Joanna Agustin", "Bonkoungou Amadou", "Branyan Brandon Wade", "Brown Steven",
                          "Butler Kyle Lee", "Canty Bryon Allen", "Cecil Shawn Eric", "Chavez Raul Daniel", "" +
                          "Choi Sung Ahm", "Cole Adam J", "Colonhernandez Joel", "Darby Jerron Lazay", "Davis Kirk"};

        Deque<String> zasadaciPoriadok = new LinkedList<>(Arrays.asList(realnyVojaci));
        if(pocetUcebnic > realnyVojaci.length) {
            System.out.println("Ucebnicu dostanu vsetci vojaci");
        } else {
            for (int i = 0; i < pocetUcebnic; i++) {
                System.out.println("Vojak " + zasadaciPoriadok.pollLast() + " dostane ucebnicu");
            }
            System.out.println();
            while(zasadaciPoriadok.size() > 0) {
                System.out.println("Vojakovi " + zasadaciPoriadok.pollLast() + " sa nezvysila ucebnica");
            }
        }
    }
}

