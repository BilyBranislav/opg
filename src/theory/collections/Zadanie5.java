package theory.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import static java.lang.Math.*;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 15.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie5 {
    private static BufferedReader br;
    private static int prijem = 0, vydaj = 0, najvyssiPrijem = 0, najvyssieSpending = 0, najlepsiMesiac = 0, najhorsiMesiac = 0;
    private static LinkedList<Integer> peniaze;

    public static void main(String[] args) {
        String [] mesiace = naplnMesiace();
        peniaze = new LinkedList<>();
        peniaze = nacitajSubor(peniaze);
        if(peniaze != null) {
            for (int i = 0; i < 24; i++) {
                vytriedPeniaze(i);
            }
            System.out.println(vyslednaSprava(mesiace));
        } else {
            System.out.println("Nastal problem");
        }
    }

    private static LinkedList<Integer> nacitajSubor(LinkedList<Integer> peniaze) {
        try {
            br = new BufferedReader(new FileReader("financie.txt"));
            String mesiac;
            String [] peniaz;
            for (int i = 0; i < 12; i++) {
                mesiac = br.readLine();
                peniaz = mesiac.split(" ");
                peniaze.add(Integer.parseInt(peniaz[0]));
                peniaze.add(Integer.parseInt(peniaz[1]));
            }
            br.close();
            return peniaze;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String[] naplnMesiace() {
        String [] mesiace = {"Januari", "Februari", "Marci", "Aprili",
                            "Maji", "June", "July", "Auguste", "Septembri",
                            "Oktobri", "Novembri", "Decembri"};
        return mesiace;
    }

    private static String vyslednaSprava(String [] mesiace) {
        return "Celkovy prijem bol: " + prijem + "\nCelkovy vydaj bol: " + vydaj +
                "\nBalanc uctu: " + (prijem - vydaj) +
                "\nNajvyssi prijem: " + najvyssiPrijem + " v " + mesiace[najlepsiMesiac] + "\n" +
                "Najvysie minutie: " +  najvyssieSpending + " v " + mesiace[najhorsiMesiac];
    }

    private static void vytriedPeniaze(int i) {
        int peniaz = peniaze.get(i);
        if(i % 2 == 0) {
            prijem += peniaz;
            if(peniaz >  najvyssiPrijem) {
                najvyssiPrijem = peniaz;
                najlepsiMesiac = i / 2;
            }
        } else {
            if(peniaz > najvyssieSpending) {
                najvyssieSpending = peniaz;
                najhorsiMesiac = i / 2;
            }
            vydaj += peniaz;
        }
    }
}