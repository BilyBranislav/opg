package theory.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 18.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie6 {

    static EvidenciaZamestnancov evidenciaZamestnancov;

    public static void main(String[] args) {
        evidenciaZamestnancov = new EvidenciaZamestnancov();
        nacitajZamestnancov();
        Zamestanenec Ferko = new Zamestanenec(5, "Ferko", "Berko", 21, 400);
        evidenciaZamestnancov.pridajZamestnanca(Ferko);
        evidenciaZamestnancov.vypisVsetkychZamestnancov();
        evidenciaZamestnancov.vypisZamestnanca(Ferko);
        System.out.println("Priemerny vek: " + evidenciaZamestnancov.getPriemernyVek() + "\n");
        System.out.println("Priemerny plat: " + evidenciaZamestnancov.getPriemernyPlat() + "\n");
        evidenciaZamestnancov.starsiAkoPriemernyVek();
        evidenciaZamestnancov.platVyssiAkoPriemernyPlat();
        evidenciaZamestnancov.vymazZamestnanca(Ferko);
        evidenciaZamestnancov.vymazZamestnanca(1);
        evidenciaZamestnancov.vypisVsetkychZamestnancov();
    }

    public static void nacitajZamestnancov() {
        //Pozri zamestnanci.txt pre strukturu nacitanych udajov
        try {
            BufferedReader br = new BufferedReader(new FileReader("zamestnanci.txt"));
            String vstup;
            String [] vstupy;
            while((vstup = br.readLine()) != null) {
                vstupy = vstup.split(" ");
                int evidencneCislo = Integer.parseInt(vstupy[0]);
                int vek = Integer.parseInt(vstupy[3]);
                double plat = Double.parseDouble(vstupy[4]);
                Zamestanenec zamestanenec = new Zamestanenec(evidencneCislo, vstupy[1], vstupy[2], vek, plat);
                evidenciaZamestnancov.pridajZamestnanca(zamestanenec);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Zamestanenec {
    private int evidencneCislo;
    private String krstneMeno;
    private String priezvisko;
    private int vek;
    private double plat;

    public Zamestanenec(int evidencneCislo, String krstneMeno, String priezvisko, int vek, double plat) {
        this.evidencneCislo = evidencneCislo;
        this.krstneMeno = krstneMeno;
        this.priezvisko = priezvisko;
        this.vek = vek;
        this.plat = plat;
    }

    public int getEvidencneCislo() {
        return evidencneCislo;
    }

    public String getKrstneMeno() {
        return krstneMeno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public int getVek() {
        return vek;
    }

    public double getPlat() {
        return plat;
    }
    @Override
    public String toString() {
        return getEvidencneCislo() + " " + getKrstneMeno() + " " + getPriezvisko() + " " + getVek() + " " + getPlat() + "€";
    }
}

class EvidenciaZamestnancov{
    private double celkovyVek = 0;
    private double celkovyPlat= 0;
    private Map<Integer, Zamestanenec> zamestnanci;

    EvidenciaZamestnancov() {
        zamestnanci = new LinkedHashMap<>();
    }

    public void pridajZamestnanca(Zamestanenec zamestnanec) {
        //Prejde celym Map a zisti ci nejaky zamestnanec ma rovnake cislo ako ten ktoreho chceme pridat
        //Ak ano, tak sa nic nestane, ak nie tak ho pridame
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            if(entry.getKey() == zamestnanec.getEvidencneCislo()) {
                System.out.println("Zamestnanec s tymto evidencnym cislom uz existuje");
                return;
            }
        }
        zamestnanci.put(zamestnanec.getEvidencneCislo(), zamestnanec);
        celkovyVek += zamestnanec.getVek();
        celkovyPlat += zamestnanec.getPlat();
    }

    public void vymazZamestnanca(Zamestanenec zamestanenec) {
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            if(entry.getValue().equals(zamestanenec)) {
                zamestnanci.remove(zamestanenec.getEvidencneCislo());
                return;
            }
        }
        System.out.println("Takyto zamestanec neexistuje");
    }

    public void vymazZamestnanca(int evidencneCislo) {
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            if(entry.getKey() == evidencneCislo) {
                zamestnanci.remove(evidencneCislo);
                return;
            }
        }
        System.out.println("Takyto zamestanec neexistuje");
    }

    public void vypisVsetkychZamestnancov() {
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            System.out.println(entry.getValue().toString());
        }
        System.out.println();
    }

    public void vypisZamestnanca(Zamestanenec zamestanenec) {
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            if(entry.getValue().equals(zamestanenec)) {
                System.out.println(zamestanenec.toString() + "\n");
            }
        }
    }

    public double getPriemernyVek() {
        return (double) Math.round(celkovyVek / zamestnanci.size() * 100) / 100;
    }

    public double getPriemernyPlat() {
        return (double) Math.round(celkovyPlat / zamestnanci.size() * 100) / 100;
    }

    public void starsiAkoPriemernyVek() {
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            if(entry.getValue().getVek() > getPriemernyVek()) {
                System.out.println(entry.getValue().toString());
            }
        }
        System.out.println();
    }

    public void platVyssiAkoPriemernyPlat() {
        for (Map.Entry<Integer, Zamestanenec> entry : zamestnanci.entrySet()) {
            if(entry.getValue().getPlat() > getPriemernyPlat()) {
                System.out.println(entry.getValue().toString());
            }
        }
        System.out.println();
    }
}
