package theory.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 18.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie7 {

    static List<Double> hody;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nacitaj pocet hodov: ");
        int pocetHodov = sc.nextInt();
        hody = new ArrayList<>(pocetHodov);
        for (int i = 0; i < pocetHodov; i++) {
            hody.add(sc.nextDouble());
        }

        System.out.println(najdlhsiHod() + " " + priemernyHod());

    }

    public static double najdlhsiHod() {
        double najdlhsiHod = 0;
        for (double hod :
                hody) {
            if(hod > najdlhsiHod) {
                najdlhsiHod = hod;
            }
        }
        return najdlhsiHod;
    }

    public static double priemernyHod() {
        double sucetHodov = 0;
        for (double hod :
                hody) {
            if(hod > 0) {
                sucetHodov += hod;
            }
        }
        return (double) Math.round(sucetHodov / hody.size() * 100) / 100;
    }
}
