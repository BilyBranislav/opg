package theory.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 18.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Zadanie8 {

    static List<String> mena;

    public static void main(String[] args) {
        mena = new ArrayList<>();
        nacitajMena();
        vypisMena();
        vypisPocetPodlaZadaniaViesCoMyslim();
    }

    public static void nacitajMena() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("mena.txt"));
            String meno;
            while((meno = br.readLine()) != null) {
                mena.add(meno);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void vypisMena() {
        Collections.sort(mena);
        for (String meno :
                mena) {
            System.out.println(meno);
        }
    }
    
    public static void vypisPocetPodlaZadaniaViesCoMyslim() {
        int pocetAPoJ = 0;
        int pocetOdKPoZ = 0;
        for (String meno :
                mena) {
            if(meno.charAt(0) >= 'A' && meno.charAt(0) <= 'J' ) {
                pocetAPoJ++;
            } else if(meno.charAt(0) >= 'K' && meno.charAt(0) <= 'Z') {
                pocetOdKPoZ++;
            }
        }
        System.out.println(pocetAPoJ + " " + pocetOdKPoZ);
    }
}
