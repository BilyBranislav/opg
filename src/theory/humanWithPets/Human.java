package theory.humanWithPets;

public class Human {

    private String firstName;
    private String lastName;
    private Pet pet = null;

    public Human(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Human(String firstName, String lastName, Pet pet) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pet = pet;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Pet getPet() {
        return pet;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String introduction() {
        try {
            return "My first name is " + firstName + ", my last name is " + lastName +
                    ", I have a pet, he is a " + pet.getBreed() + "  and his name is " + pet.getName();
        } catch (NullPointerException e){
            return "My first name is " + firstName + ", my last name is " + lastName + " and I don't have a pet";
        }
    }
}
