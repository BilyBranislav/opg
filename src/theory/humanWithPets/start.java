package theory.humanWithPets;

public class start {
    public static void main(String[] args) {
        Pet branosDog = new Pet("Dixo", "dog");
        Human brano = new Human("Brano", "Bily", branosDog);
        Pet ferosDog = new Pet("Velvet", "cat");
        Human fero = new Human("Fero", "Gero", ferosDog);
        Human karo = new Human("Karo", "Jole");

        String branosIntroduction = brano.introduction();
        String ferosIntroduction = fero.introduction();
        String karosIntroducion = karo.introduction();

        System.out.println(branosIntroduction);
        System.out.println(ferosIntroduction);
        System.out.println(karosIntroducion);

        Pet karosMys = new Pet("Velvet", "mouse");
        karo.setPet(karosMys);

        karosIntroducion = karo.introduction();
        System.out.println(karosIntroducion);
    }
}
