package theory.readingFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by Branislav Bilý on 08.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class readingFile {

    private static BufferedReader br;

    public static void main(String[] args) {
        String input = wholeString();
        System.out.println(input);
        if(input != null) {
            String[] numbers = numbersInput(Integer.parseInt(input));
            System.out.println(Arrays.toString(numbers));
        }
    }

    private static String wholeString() {
        try {
            br = new BufferedReader(new FileReader("input.txt"));
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String[] numbersInput(int amount) {
        String numbers[] = new String[amount];
        String number;
        try {
            number = br.readLine();
            numbers = number.split(" ");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("No such index found");
        }
        return numbers;
    }
}
