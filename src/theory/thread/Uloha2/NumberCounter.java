package theory.thread.Uloha2;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 27.02.2019
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class NumberCounter {
    private int count = -1;

    synchronized int count() {
        count++;
        return count;
    }
}
