package theory.thread.Uloha2;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 27.02.2019
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class NumberGenerator extends Thread {
    private final NumberCounter numberCounter = new NumberCounter();
    private boolean running = true;

    @Override
    public void run() {
        while (running) {
            System.out.println(numberCounter.count());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("We done here bois");
    }

    void setRunning(boolean running) {
        this.running = running;
    }
}

