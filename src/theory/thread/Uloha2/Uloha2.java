package theory.thread.Uloha2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 27.02.2019
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Uloha2 {
    public static void main(String[] args) {

        NumberGenerator numberGenerator = new NumberGenerator();
        numberGenerator.start();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        char firstLetter = 'M';
        try {
            while(firstLetter != 'K') {
                input = br.readLine();
                if(!input.equals("")) {
                    firstLetter = input.charAt(0);
                }
            }
            numberGenerator.setRunning(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


